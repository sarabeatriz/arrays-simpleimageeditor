#-------------------------------------------------
#
# Project created by QtCreator 2014-03-13T15:27:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleImageEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    filter.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    images.qrc

