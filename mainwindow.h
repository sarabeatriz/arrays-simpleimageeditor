// RAN 2015-07-03 - Moved some prototypes to filter.h

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QSlider>
#include <QShortcut>
#include <QMessageBox>

#define WHITE 0xffffffff
#define BLACK 0xff000000

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /// \fn MainWindow::MainWindow(QWidget *parent)
    /// \~English
    /// \brief Constructor
    /// \~Spanish
    /// \brief Constructor
    explicit MainWindow(QWidget *parent = 0);

    /// \fn void MainWindow::GrayScale(QImage &editedImage)
    /// \~English
    /// \brief Function that applies a greyscale filter in the edited image.
    /// It works by turning each pixel into the color grey.
    /// \param originalImage Reference to the original image (no filter)
    /// \param editedImage Reference to the destination image with the filter applied.
    /// \~Spanish
    /// \brief Funcion que aplica un filtro de escala gris en la imagen editada.
    /// Funciona cambiando cada pixel a un tono de color gris.
    /// \param originalImage Referencia a la imagen original (sin filtro)
    /// \param editedImage Referencia a la imagen destino con el filtro aplicado.
    void GrayScale(QImage &);

    /// \fn void MainWindow::VerticalFlip(QImage &editedImage)
    /// \~English
    /// \brief Function that applies a vertical flip to the edited image.
    /// It works by putting the pixel from the original image in the heigth-1-j
    /// position of the edited image.
    /// \param originalImage Reference to the original image (no filter)
    /// \param editedImage Reference to the destination image with the filter applied.
    /// \~Spanish
    /// \brief Funcion que aplica un filtro de flip vertical en la imagen editada.
    /// Funciona poniendo el pixel de la imagen original en la posicion height-1-j (altura)
    /// de la imagen editada.
    /// \param editedImage Referencia a la imagen destino con el filtro aplicado.
    void VerticalFlip(QImage &);

    /// \fn void MainWindow::HorizontalFlip(QImage &editedImage)
    /// \~English
    /// \brief Function that applies a horizontal flip to the edited image.
    /// It works by putting the pixel from the original image in the width-1-i
    /// position of the edited image.
    /// \param originalImage Reference to the original image (no filter)
    /// \param editedImage Reference to the destination image with the filter applied.
    /// \~Spanish
    /// \brief Funcion que aplica un filtro de flip horizontal en la imagen editada.
    /// Funciona poniendo el pixel de la imagen original en la posicion width-1-i (altura)
    /// de la imagen editada.
    /// \param editedImage Referencia a la imagen destino con el filtro aplicado.
    void HorizontalFlip(QImage &);

    /// \fn void MainWindow::ThresholdFilter(QImage &originalImage, QImage &editedImage,unsigned int threshold, bool color)
    /// \~English
    /// \brief Function that applies a theshold filter to the edited image.
    /// It works by creating a black and white image where the color of the new pixel
    /// is white or black depending on a threshold value and the average of the colors
    /// of the pixel of the original image.
    /// \param originalImage Reference to the original image (no filter)
    /// \param editedImage Reference to the destination image with the filter applied.
    /// \param threshold Integer value that serves as a black or white color threshold
    /// \param color boolean value to select which color white or black if the color average
    /// is under or over the threshold.
    /// \~Spanish
    /// \brief Funcion que aplica un filtro de umbral en la imagen editada.
    /// Funciona creando una imagen en blanco y negro donde el color del pixel nuevo is
    /// blanco o negro dependiendo de un valor umbral y el promedio de los colores del pixel
    /// de la imagen original.
    /// \param originalImage Referencia a la imagen original (sin filtro)
    /// \param editedImage Referencia a la imagen destino con el filtro aplicado.
    /// \param threshold Intero que sirve como un umbral de color entre blanco y negro
    /// \param color Booleano para seleccionar que color blanco o negro si el promedio
    /// del color esta bajo o sobre el umbral.
    void ThresholdFilter(QImage &, QImage &,unsigned int , bool);

    /// \fn MainWindow::MainWindow(QWidget *parent)
    /// \~English
    /// \brief Destructor
    /// \~Spanish
    /// \brief Destructor
    ~MainWindow();

private slots:

    /// \fn void MainWindow::on_actionLoad_Image_triggered()
    /// \~English
    /// \brief Invokes function on_btnLoadImage_clicked()
    /// \~Spanish
    /// \brief Invoca la funcion on_btnLoadImage_clicked()
    void on_actionLoad_Image_triggered();

    /// \fn void MainWindow::on_actionSave_Image_triggered()
    /// \~English
    /// \brief Invokes function on_btnSaveImage_clicked()
    /// \~Spanish
    /// \brief Invoca la funcion  on_btnSaveImage_clicked()
    void on_actionSave_Image_triggered();

    /// \fn void MainWindow::on_actionClose_Image_triggered()
    /// \~English
    /// \brief Invokes function close()
    /// \~Spanish
    /// \brief Invoca la funcion  close()
    void on_actionClose_Window_triggered();

    /// \fn void MainWindow::on_actionInvert_Threshold_Colors_triggered()
    /// \~English
    /// \brief Invokes function on_btnInvertThreshold_clicked()
    /// \~Spanish
    /// \brief Invoca la funcion on_btnInvertThreshold_clicked()
    void on_actionInvert_Threshold_Colors_triggered();

    /// \fn void MainWindow::on_actionFlip_Image_Horizontally_triggered()
    /// \~English
    /// \brief Invokes function on_btnFlipImageHorizontally_clicked()
    /// \~Spanish
    /// \brief Invoca la funcion on_btnFlipImageHorizontally_clicked()
    void on_actionFlip_Image_Horizontally_triggered();

    /// \fn void MainWindow::on_actionFlip_Image_Vertically_triggered()
    /// \~English
    /// \brief Invokes function on_btnFlipImageVertically_clicked()
    /// \~Spanish
    /// \brief Invoca la funcion on_btnFlipImageVertically_clicked()
    void on_actionFlip_Image_Vertically_triggered();

    /// \fn void MainWindow::on_actionApply_Grey_Scale_Filter_triggered()
    /// \~English
    /// \brief Invokes function on_btnGreyScaleFilter_clicked()
    /// \~Spanish
    /// \brief Invoca la funcion on_btnGreyScaleFilter_clicked()
    void on_actionApply_Grey_Scale_Filter_triggered();

    /// \fn void MainWindow::on_actionRevert_Edited_Image_to_Original_triggered()
    /// \~English
    /// \brief Invokes function on_btnRevertImage_clicked()
    /// \~Spanish
    /// \brief Invoca la funcion on_btnRevertImage_clicked()
    void on_actionRevert_Edited_Image_to_Original_triggered();

    /// \fn void MainWindow::on_actionInstructions_triggered()
    /// \~English
    /// \brief To display the instructions.
    /// \~Spanish
    /// \brief Para desplegar las instrucciones.
    void on_actionInstructions_triggered();


    /// \fn void MainWindow::applyThresholdFilter()
    /// \~English
    /// \brief To apply the threshold filter.
    /// \~Spanish
    /// \brief Para aplicar el filtro de umbral.
    void applyThresholdFilter();

    /// \fn void MainWindow::instructions()
    /// \~English
    /// \brief To display the instructions.
    /// \~Spanish
    /// \brief Para desplegar las instrucciones.
    void instructions();


    /// \fn void MainWindow::on_thresholdSlider_sliderReleased()
    /// \~English
    /// \brief To apply the threshold filter and load the edited image to the GUI when
    /// the GUI slider is moved.
    /// \~Spanish
    /// \brief Para aplicar el filtro de umbral y cargar la imagen editada al GUI cuando
    /// el deslizador del GUI es movido.
    void on_thresholdSlider_sliderReleased();

    /// \fn void MainWindow::on_btnLoadImage_clicked()
    /// \~English
    /// \brief To load an image to the GUI when the load image option is selected.
    /// \~Spanish
    /// \brief Para cargar una imagen al GUI cuando se marca opcion de cargar imagen.
    void on_btnLoadImage_clicked();

    /// \fn void MainWindow::on_btnSaveImage_clicked()
    /// \~English
    /// \brief To save an image to the file system when the save image option is selected.
    /// \~Spanish
    /// \brief Para guardar una imagen al sistema de archivos cuando se marca opcion de guardar imagen.
    void on_btnSaveImage_clicked();

    /// \fn void MainWindow::on_chboxThreshold_clicked()
    /// \~English
    /// \brief To apply the threshold filter and load the edited image to the GUI.
    /// \~Spanish
    /// \brief Para aplicar el filtro de umbral y cargar la imagen editada al GUI.
    void on_chboxThreshold_clicked();

    /// \fn void MainWindow::on_btnInvertThreshold_clicked()
    /// \~English
    /// \brief To apply the threshold filter and load the edited image to the GUI when
    /// the threshold color is inverted.
    /// \~Spanish
    /// \brief Para aplicar el filtro de umbral y cargar la imagen editada al GUI cuando
    /// el color del umbral es invertido.
    void on_btnInvertThreshold_clicked();

    /// \fn void MainWindow::on_btnFlipImageHorizontally_clicked()
    /// \~English
    /// \brief To call the horizontal flip image filter.
    /// \~Spanish
    /// \brief Para llamar al filtro de imagen flip horizontal.
    void on_btnFlipImageHorizontally_clicked();

    /// \fn void MainWindow::on_btnFlipImageVertically_clicked()
    /// \~English
    /// \brief To call the vertical flip image filter.
    /// \~Spanish
    /// \brief Para llamar al filtro de imagen flip vertical.
    void on_btnFlipImageVertically_clicked();

    /// \fn void MainWindow::on_btnGreyScaleFilter_clicked()
    /// \~English
    /// \brief To call the Gray Scale image filter.
    /// \~Spanish
    /// \brief Para llamar al filtro de imagen de escalas grises.
    void on_btnGreyScaleFilter_clicked();

    /// \fn void MainWindow::on_btnRevertImage_clicked()()
    /// \~English
    /// \brief To reset the edited image to the original image.
    /// \~Spanish
    /// \brief Para reajustar la imagen editada a la imagen original.
    void on_btnRevertImage_clicked();

private:
    Ui::MainWindow *ui;
    QImage originalImage;  //This will hold the original image.
    QImage editedImage;    //This will hold the edited image.
    bool invertColor;   //Boolean value of the switch button for the threshold filter.
};

#endif // MAINWINDOW_H
